import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  isLoading: boolean = false;
  loginError: string;

  constructor(private session: SessionService, private router: Router, private auth: AuthService) {

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }

  }

  ngOnInit(): void {
  }

  get username() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password');
  }

  async onLoginClicked() {

    this.loginError = '';

    try {
      this.isLoading = true;
      const { token, username } = await this.auth.login(this.loginForm.value);
      this.session.save({ token: token, username: username });
      this.router.navigateByUrl('/dashboard');
    } catch (e) {
      this.loginError = e;
    } finally {
      this.isLoading = false;
    }
  }

}
